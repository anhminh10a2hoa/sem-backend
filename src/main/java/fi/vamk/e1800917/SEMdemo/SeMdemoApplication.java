package fi.vamk.e1800956.SEMdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeMdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeMdemoApplication.class, args);
	}

}
